package fr.umfds.agl.tpposte;

import static org.assertj.core.api.Assertions.assertThat; 
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import poste.Lettre;
import poste.Recommandation;

class LettreTest {

	Lettre lettre1;
	Lettre lettre2;
	
	private static float tolerancePrix=0.001f;
	
	@BeforeEach
	public void initialisation() {
		
		lettre1 = new Lettre("Le pere Noel",
				"famille Kirik, igloo 5, banquise nord",
				"7877", 25, 0.00018f, Recommandation.un, false);
	}
	
	@Test
	public void tarifAffranchissementTest() {
		
		float tarifAff= lettre1.tarifAffranchissement();
		
		assertTrue(Math.abs(tarifAff-1.0f)<tolerancePrix,"Tarif affranchissemnt NOK");
	}
	
	@Test
	public void tarifRemboursementTest(){
		
		float tarifRemb = lettre1.tarifRemboursement();
		
		assertTrue(Math.abs(tarifRemb-1.5f)<tolerancePrix,"Tarif remboursement NOK");
	}
	
	//deuxième version de tarifRemboursementTestAvecAssertJ() avec AssertJ
	@Test
	public void tarifRemboursementTestAvecAssertJ() {
		
		float tarifRemb = lettre1.tarifRemboursement();
		
		assertThat(Math.abs(tarifRemb-1.5f)).withFailMessage("Tarif remboursement NOK")
											.isLessThan(tolerancePrix);
	}
	
	@Test
	public void toStringTest() {
		
		String description = lettre1.toString();
		
		assertEquals(description,"Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire");
	}

	//deuxième version de toStringTest() avec AssertJ
	@Test
	public void toStringTestAvecAssertJ() {
		
		String description = lettre1.toString();
		
		assertThat(description).isEqualTo("Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire");
	}
}
