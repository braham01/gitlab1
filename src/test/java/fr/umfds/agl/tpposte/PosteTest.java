package fr.umfds.agl.tpposte;





import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import poste.Colis;
import poste.Lettre;
import poste.Recommandation;

/**
 * Unit test for simple App.
 */
public class PosteTest 
{
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	
	Lettre lettre1;
	Lettre lettre2;
	Colis colis1;
	@BeforeEach
	public void createObjects() {
		lettre1 = new Lettre("Le pere Noel",
				"famille Kirik, igloo 5, banquise nord",
				"7877", 25, 0.00018f, Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel",
				"famille Kouk, igloo 2, banquise nord",
				"5854", 18, 0.00018f, Recommandation.deux, true);
		colis1 = new Colis("Le pere Noel", 
				"famille Kaya, igloo 10, terres ouest",
				"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
	}
	
    @Test
    public void toStringTest()
    {
        assertEquals(colis1.toString(),"Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0","toString Colis 1 NOK");
        assertEquals(lettre1.toString(),"Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire","toString Lettre 1 NOK");
        assertEquals(lettre2.toString(),"Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence","toString Lettre 2 NOK");
    }
    
    @Test
    public void affranchissementTest()
    {
    	assertEquals(1.0f,lettre1.tarifAffranchissement(),tolerancePrix,()->"Affranchissement lettre 1 NOK");
    	assertEquals(2.3f,lettre2.tarifAffranchissement(),tolerancePrix,()->"Affranchissement lettre 2 NOK");    
    	assertEquals(3.5f,colis1.tarifAffranchissement(),tolerancePrix,()->"Affranchissement colis 1 NOK");
    }
    
    @Test
    public void remboursementTest()
    {
    	assertEquals(1.5f,lettre1.tarifRemboursement(),tolerancePrix,()->"Remboursement lettre 1 NOK");
    	assertEquals(15.0f,lettre2.tarifRemboursement(),tolerancePrix,()->"Remboursement lettre 2 NOK");    
    	assertEquals(100.0f,colis1.tarifRemboursement(),tolerancePrix,()->"Remboursement colis 1 NOK");
    }
    
}
