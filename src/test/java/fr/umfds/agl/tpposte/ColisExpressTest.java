package fr.umfds.agl.tpposte;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import poste.ColisExpress;
import poste.ColisExpressInvalide;
import poste.Recommandation;

public class ColisExpressTest {
	
	ColisExpress express1;
	
	@Test
	public void construire_un_colis_express_avec_poids_invalide_test() {
		
		assertThrows(ColisExpressInvalide.class,()->{
			new ColisExpress("Le pere Noel","famille Kaya, igloo 10, terres ouest", 
					"7877", 40, 0.2f, Recommandation.deux, "train electrique",0.2f,true);
			});
	}
	
	@Test
	public void construire_un_colis_express_avec_poids_invalide_test_assertJ() {
		
		assertThatExceptionOfType(ColisExpressInvalide.class).isThrownBy(()->{
			new ColisExpress("Le pere Noel","famille Kaya, igloo 10, terres ouest", 
					"7877", 40, 0.2f, Recommandation.deux, "train electrique",0.2f,true);
			});
	}
	
	@Test
	public void tarifAffranchissementTestSansAmbalageTest() {
		
		try {
			
			express1 = new ColisExpress("Le pere Noel","famille Kaya, igloo 10, terres ouest", 
				"	7877", 28, 0.2f, Recommandation.deux, "train electrique",0.2f,false);
			
			float tarifAff = express1.tarifAffranchissement();
			
			assertEquals(tarifAff,30);
			
		}catch(Exception e) {
			
			fail();
		}
	}
	
	@Test
	public void tarifAffranchissementTestAvecAmbalageTest() {
		
		try {
			
			express1 = new ColisExpress("Le pere Noel","famille Kaya, igloo 10, terres ouest", 
				"	7877", 28, 0.2f, Recommandation.deux, "train electrique",0.2f,true);
			
			float tarifAff = express1.tarifAffranchissement();
			
			assertEquals(tarifAff,33);
			
		}catch(Exception e) {
			
			fail();
		}
	}
	
	
	@Test
	public void toStringTest() {
		
		try {
			express1 = new ColisExpress("Le pere Noel","famille Kaya, igloo 10, terres ouest", 
					"	7877", 28, 0.2f, Recommandation.deux, "train electrique",0.2f,true);
			
			String description = express1.toString();
			
			assertEquals(description,"Colis express 	7877/famille Kaya, igloo 10, terres ouest/2/0.2/0.2/28.0/2");
		}catch(Exception e) {
			
			fail();
		}
		
	}
}