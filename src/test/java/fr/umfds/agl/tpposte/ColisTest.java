package fr.umfds.agl.tpposte;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import poste.Colis;
import poste.Recommandation;

public class ColisTest {
	
	Colis colis;
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;

	@BeforeEach
	public void creation_objets() {
		colis = new Colis("Le pere Noel", 
				"famille Kaya, igloo 10, terres ouest",
				"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
	}
	
	@Test
	public void getOrigineTest() {
		
		String origine = colis.getOrigine();
		
		assertEquals(origine,"Le pere Noel");
	}
	
	@Test
	public void getDestinationTest() {
		
		String destination = colis.getDestination();
		
		assertEquals(destination,"famille Kaya, igloo 10, terres ouest");
	}
	
	@Test
	public void getCodePostalTest() {
		
		String CodePostal = colis.getCodePostal();
		
		assertEquals(CodePostal,"7877");
	}
	
	@Test
	public void getPoidsTest() {
		
		float poids = colis.getPoids();
		
		assertEquals(poids,1024);
	}
	
	@Test
	public void getVolumeTest() {
		
		float volume = colis.getVolume();
		
		assertEquals(volume,0.02f);
	}
	
	@Test
	public void getTauxRecommandationTest() {
		
		Recommandation recommandation = colis.getTauxRecommandation();
		
		assertEquals(recommandation,Recommandation.deux);
	}

	@Test
	public void getDeclareContenuTest() {
		
		String contenu = colis.getDeclareContenu();
		
		assertEquals(contenu,"train electrique");
	}
	
	@Test
	public void getValeurDeclareeTest() {
		
		float valeurDeclaree = colis.getValeurDeclaree();
		
		assertEquals(valeurDeclaree,200);
	}
	
	@Test 
	public void getTarifBaseTest() {
		
		float tarifBase = colis.getTarifBase();
		
		assertEquals(tarifBase,2);
	}
	
	@Test 
	public void tarifAffranchissementTest() {
		
		float tarifRemb = colis.tarifAffranchissement();
		
		assertTrue((Math.abs(tarifRemb-3.5f))<tolerancePrix,"Affranchissement colis NOK");
	}
	
	@Test
	public void tarifRemboursementTest() {
		
		float tarifRemb = colis.tarifRemboursement();
		
		assertTrue((Math.abs(tarifRemb-100.0f))<tolerancePrix,"Remboursement colis NOK");
	}
	
	@Test
	public void toStringTest() {

		String description = colis.toString();
		
		assertEquals(description,"Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0");
	}
}
